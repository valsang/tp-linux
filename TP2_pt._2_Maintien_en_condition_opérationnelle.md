# TP2 pt. 2 : Maintien en condition opérationnelle
## I. Monitoring
### 2. Setup
- Manipulation du service Netdata
```
[kalop@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 09:52:53 CEST; 46s ago
  Process: 2279 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/run/netdata (code=exited, status=0/SUCCESS)
  Process: 2277 ExecStartPre=/bin/mkdir -p /opt/netdata/var/run/netdata (code=exited, status=0/SUCCESS)
  Process: 2275 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/cache/netdata (code=exited, status=0/SUCCES>
  Process: 2274 ExecStartPre=/bin/mkdir -p /opt/netdata/var/cache/netdata (code=exited, status=0/SUCCESS)
 Main PID: 2281 (netdata)
    Tasks: 31 (limit: 4946)
   Memory: 74.9M
   CGroup: /system.slice/netdata.service
           ├─2281 /opt/netdata/bin/srv/netdata -P /opt/netdata/var/run/netdata/netdata.pid -D
           ├─2290 /opt/netdata/bin/srv/netdata --special-spawn-server
           ├─2450 /opt/netdata/usr/libexec/netdata/plugins.d/go.d.plugin 1
           └─2455 /opt/netdata/usr/libexec/netdata/plugins.d/apps.plugin 1

Oct 11 09:52:53 web.tp2.linux [2281]: CONFIG: cannot load user config '/opt/netdata/etc/netdata/netdata.conf'. Will try>
Oct 11 09:52:53 web.tp2.linux netdata[2281]: 2021-10-11 09:52:53: netdata INFO  : MAIN : CONFIG: cannot load user confi>
Oct 11 09:52:53 web.tp2.linux [2281]: CONFIG: cannot load stock config '/opt/netdata/usr/lib/netdata/conf.d/netdata.con>
Oct 11 09:52:53 web.tp2.linux netdata[2281]: 2021-10-11 09:52:53: netdata INFO  : MAIN : CONFIG: cannot load stock conf>
Oct 11 09:52:53 web.tp2.linux [2281]: CONFIG: cannot load cloud config '/opt/netdata/var/lib/netdata/cloud.d/cloud.conf>
Oct 11 09:52:53 web.tp2.linux netdata[2281]: 2021-10-11 09:52:53: netdata INFO  : MAIN : CONFIG: cannot load cloud conf>
Oct 11 09:52:53 web.tp2.linux [2281]: Found 0 legacy dbengines, setting multidb diskspace to 256MB
Oct 11 09:52:53 web.tp2.linux netdata[2281]: 2021-10-11 09:52:53: netdata INFO  : MAIN : Found 0 legacy dbengines, sett>
Oct 11 09:52:53 web.tp2.linux [2281]: Created file '/opt/netdata/var/lib/netdata/dbengine_multihost_size' to store the >
Oct 11 09:52:53 web.tp2.linux netdata[2281]: 2021-10-11 09:52:53: netdata INFO  : MAIN : Created file '/opt/netdata/var>

[kalop@web ~]$ sudo systemctl is-enabled netdata
enabled
[kalop@web ~]$
```
- autoriser ce port dans le firewall
```
[kalop@web ~]$ sudo firewall-cmd --remove-port=8888/tcp --permanent
success
[kalop@web ~]$ sudo firewall-mcd --reload
```
## II. Backup
### 2. Partage NFS

- Setup environnement
```
[kalop@backup backup]$ ls
db.tp2.linux  web.tp2.linux
[kalop@backup backup]$
```

- Setup partage NFS
```
[kalop@backup ~]$ sudo vi /etc/idmapd.conf
[sudo] password for kalop:
[kalop@backup ~]$ sudo vi /etc/exports
[kalop@backup ~]$ [kalop@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[kalop@backup ~]$ sudo direwall-cmd --add-service=nfs --permanent
sudo: direwall-cmd: command not found
[kalop@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[kalop@backup ~]$ sudo firewall-cmd --reload
success
```

-  Setup points de montage sur web.tp2.linux
```
[kalop@web ~]$ sudo mount -t nfs 10.102.1.13:/srv/backup/web.tp2.linux/ /srv/backup/
[kalop@web ~]$ df -hT
Filesystem                            Type      Size  Used Avail Use% Mounted on
devtmpfs                              devtmpfs  387M     0  387M   0% /dev
tmpfs                                 tmpfs     405M  344K  405M   1% /dev/shm
tmpfs                                 tmpfs     405M  5.6M  400M   2% /run
tmpfs                                 tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                   xfs       6.2G  3.8G  2.5G  61% /
/dev/sda1                             xfs      1014M  301M  714M  30% /boot
tmpfs                                 tmpfs      81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backup/web.tp2.linux nfs4      6.2G  2.2G  4.1G  35% /srv/backup
[kalop@web ~]$ sudo vi /etc/fstab
[kalop@web ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 15 08:30:09 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=cb5bca76-9ca0-458a-ab84-7e8a44b2280d /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0

10.102.1.13:/srv/backup/web.tp2.linux/ /srv/backup nfs  defaults        0 0
[kalop@web ~]$
```
### 3. Backup de fichiers

- /srv/tp2_backup.sh

```
#!/bin/bash
#Simple backup script
# Rukab 12/10/2021
Date=$(date +%Y%m%d)
Time=$(date +%H%M%S)
Res="tp2_backup_${Date}_${Time}.tar.gz"
Gzfile=$(tar -cvzf ${Res} ${2})
rsync -av --remove-source-files ${Res} ${1}
qty_to_keep_tail=$((5+1))
ls -tp "${1}" | grep -v '/$' | tail -n +${qty_to_keep_tail} | xargs -I {} rm -- ${1}/{}
````
-  Tester le bon fonctionnement

```
[kalop@web srv]$  sudo ./tp2_backup.sh  /srv/backup/ archtest/
[kalop@web srv]$ cd backup/
[kalop@web backup]$ ls
tp2_backup_20211012_040048.tar.gz
[kalop@web backup]$
```

```
[kalop@backup web.tp2.linux]$ ls
tp2_backup_20211012_040048.tar.gz
```



### 4. Unité de service
#### A. Unité de service
- Tester le bon fonctionnement
```
[kalop@web ~]$ sudo systemctl start tp2_backup
[kalop@web ~]$ cd /srv/backup/
[kalop@web backup]$ ls
tp2_backup_20211012_040048.tar.gz  tp2_backup_20211012_040423.tar.gz
[kalop@web backup]$
```
```
[kalop@backup web.tp2.linux]$ ls
tp2_backup_20211012_040048.tar.gz  tp2_backup_20211012_040423.tar.gz
```
#### B. Timer

- prouver que... le timer est actif actuellement qu'il est paramétré pour être actif dès que le système boot

```
[kalop@web backup]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2021-10-12 04:08:12 CEST; 34s ago
  Trigger: Tue 2021-10-12 04:09:00 CEST; 12s left

Oct 12 04:08:12 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```

```
[kalop@web backup]$ sudo systemctl is-enabled tp2_backup.timer
enabled
[kalop@web backup]$
````

- vérifiez que la backup s'exécute correctement
```
[kalop@backup web.tp2.linux]$ ls -l
total 28
-rw-r--r--. 1 root root 212 Oct 12 04:00 tp2_backup_20211012_040048.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:04 tp2_backup_20211012_040423.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:08 tp2_backup_20211012_040812.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:09 tp2_backup_20211012_040903.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:10 tp2_backup_20211012_041003.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:11 tp2_backup_20211012_041103.tar.gz
-rw-r--r--. 1 root root 215 Oct 12 04:12 tp2_backup_20211012_041203.tar.gz
[kalop@backup web.tp2.linux]$
```

#### C. Contexte

- prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15
```
[kalop@web srv]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED    UNIT                         ACTIVATES
Fri 2021-10-15 03:15:00 CEST  8h left  n/a                           n/a       tp2_backup.timer             tp2_backup.>
Fri 2021-10-15 17:57:04 CEST  23h left Thu 2021-10-14 17:57:04 CEST  19min ago systemd-tmpfiles-clean.timer systemd-tmp>
n/a                           n/a      n/a                           n/a       dnf-makecache.timer          dnf-makecac>
```

-  Fichier /etc/systemd/system/tp2_backup.timer
```
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
```
-  Fichier /etc/systemd/system/tp2_backup.service
```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup/ /var/www/sub-domains/com.yourdomain.nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

## III. Reverse Proxy
### 2. Setup simple

#### Installer NGINX
```
[kalop@front ~]$ sudo dnf install -y epel-release
```

```
[kalop@front ~]$ sudo dnf install -y nginx
```

#### Tester 
- Lancer le service nginx
```
[kalop@front ~]$ sudo start nginx
```

```
[kalop@front ~]$ sudo systemctl enable nginx
```
- repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall
```
[kalop@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

- vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC
```
PS C:\Users\rukab> curl http://10.102.1.14/                                                                             

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
```

#### Explorer la conf par défaut de NGINX

- repérez l'utilisateur qu'utilise NGINX par défaut
```
[kalop@front ~]$ sudo cat /etc/nginx/nginx.conf
[sudo] password for kalop:
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
```
- nginx utilisie l'ulisateur nginx par defaut

- par défaut, le fichier de conf principal inclut d'autres fichiers de conf mettez en évidence ces lignes d'inclusion dans le fichier de conf principal
```
include /etc/nginx/default.d/*.conf;
```

#### Modifier la conf de NGINX
- créer un fichier /etc/nginx/conf.d/web.tp2.linux.conf avec le contenu suivant
```
[kalop@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}

[kalop@front ~]$
```
## IV. Firewalling
### Mise en place
#### A. Base de données
####  Restreindre l'accès à la base de données db.tp2.linux
- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```
[kalop@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[kalop@db ~]$ sudo firewall-cmd --get-default-zone
drop
```

#### B. Serveur Web
- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```
[kalop@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32
[kalop@web ~]$ sudo firewall-cmd --get-default-zone
drop
```
#### C. Serveur de backup
- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```
[kalop@backup ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32
[kalop@backup ~]$ sudo firewall-cmd --get-default-zone
drop
[kalop@backup ~]$
```
#### D. Reverse Proxy
- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd
```
[kalop@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
net
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
[kalop@front ~]$ sudo firewall-cmd --get-default-zone
drop
[kalop@front ~]$
```

#### E. Tableau récap


| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | `80` `22`           | `10.102.1.1` `10.102.1.14`             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | `3306` `22`          | `10.102.1.1` `10.102.1.11`            |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `2049` `22`          | `10.102.1.1` `10.102.1.11` `10.102.1.12`            |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | `80` `22`           | `10.102.1.1` `10.102.1.0/24`             |
