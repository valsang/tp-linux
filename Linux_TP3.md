# Linux TP3
## I installation
- on vas donc installer prosody un serveur XMPP open source
### 1 serveur prosody

- tout d'abors il faut ajouter d'autres repo étant donné que prosody n'est pas present dans les repos de base 
```
[kalop@prosody ~]$ sudo dnf install epel-release
[kalop@prosody ~]$ sudo dnf config-manager --set-enabled powertools
[kalop@prosody ~]$ sudo dnf update
````
- ensuite on install le serveur 
```
[kalop@prosody ~]$ sudo dnf install prosody
```

- on vas modifier le fichier /etc/prosody/prosody.cfg.lua en configurant le nom du serveur, mettant en place une room de discussion, ajoutant un compte admin et autorisant l'inscription de nouveaux utilisateurs 
```
VirtualHost "palok.com"
        enabled = true




        ssl = {
                key = "/var/lib/prosody/palok.com.key"
                certificate = "/var/lib/prosody/palok.com.key"
        }
```
```
---Set up a MUC (multi-user chat) room server on conference.example.com:
Component "conference.palok.com" "muc"
        restrict_room_creation "admin"
```
```
-- Example: admins = { "user1@example.com", "user2@example.net" }
admins = {"kalop@palok.com" }
```
```
-- Nice to have
        "version"; -- Replies to server version requests
        "uptime"; -- Report how long server has been running
        "time"; -- Let others know the time here on this server
        "ping"; -- Replies to XMPP pings with pongs
        "register"; -- Allow users to register on this server using a client and change passwords
```
- On vas ensuite generer des ceritificats afin de metrre en place ssl sur le serveur 

```
[kalop@prosody ~]$ sudo prosodyctl cert generate palok.com
Choose key size (2048): 4096
Generating RSA private key, 4096 bit long modulus (2 primes)
.............................................++++
....................................................++++
e is 65537 (0x010001)
Key written to /var/lib/prosody/palok.com.key
Please provide details to include in the certificate config file.
Leave the field empty to use the default value or '.' to exclude the field.
countryName (GB): fr
localityName (The Internet):
organizationName (Your Organisation): palok.com
organizationalUnitName (XMPP Department):
commonName (palok.com):
emailAddress (xmpp@palok.com):

Config written to /var/lib/prosody/palok.com.cnf
Certificate written to /var/lib/prosody/palok.com.crt
```
- On ajoute ensuite un utilisateur l'utilisateur admin dans le cas present avant de demarrer le service 
```
[kalop@prosody ~]$ sudo prosodyctl adduser kalop@palok.com
[sudo] password for kalop:
Enter new password:
Retype new password:
[kalop@prosody ~]$ sudo systemctl start prosody
```
- Pour finir on config le firewall afin de laisser passer les connections necessaires au fonction du serveur
```
[kalop@prosody ~]$ sudo firewall-cmd --add-port=5222/tcp --permanent
[sudo] password for kalop:
success
[kalop@prosody ~]$ sudo firewall-cmd --add-port=5269/tcp --permanent
success
[kalop@prosody ~]$ sudo firewall-cmd --add-port=5280/tcp --permanent
success
[kalop@prosody ~]$ sudo firewall-cmd --reload
success
[kalop@prosody ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 5222/tcp 5269/tcp 5280/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- Une fois terminé on est sensé pouvoir ce co depuis un client xmpp je le fait avec Pidgin depuis mon windows dans l'exemple si dessous!
![](https://i.imgur.com/cqoDNFt.png)

## II Automatisation de la solution
- Dans cette deuxieme partie on vas automatiser l'instalation et la config du serveur prosody

```
#!/usr/bin/env bash
# Script for the automation of the install and config of a prosody server
# kalypso 16/11/2021
# usage ./install_prosody.sh name_of_the_host admin_name common_room_name passwd_admin

sudo dnf install epel-release -y
sudo dnf config-manager --set-enabled powertools -y
sudo dnf update -y
sudo dnf install prosody -y

sudo echo "-- Prosody XMPP Configuration File
--
-- Information on configuring Prosody can be found on our
-- website at https://prosody.im/doc/configure
--
-- Tip: You can check that the syntax of this file is correct
-- when you have finished by running this command:
--     prosodyctl check config
-- If there are any errors, it will let you know what and where
-- they are, otherwise it will keep quiet.
--
-- Good luck, and happy Jabbering!


---------- Server-wide settings ----------
-- Settings in this section apply to the whole server and are the default settings
-- for any virtual hosts

-- This is a (by default, empty) list of accounts that are admins
-- for the server. Note that you must create the accounts separately
-- (see https://prosody.im/doc/creating_accounts for info)
-- Example: admins = { "user1@example.com", "user2@example.net" }
admins = {"${2}@${1}" }

-- Enable use of libevent for better performance under high load
-- For more information see: https://prosody.im/doc/libevent
--use_libevent = true

-- Prosody will always look in its source directory for modules, but
-- this option allows you to specify additional locations where Prosody
-- will look for modules first. For community modules, see https://modules.prosody.im/
--plugin_paths = {}

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules
modules_enabled = {

        -- Generally required
                "roster"; -- Allow users to have a roster. Recommended ;)
                "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
                "tls"; -- Add support for secure TLS on c2s/s2s connections
                "dialback"; -- s2s dialback support
                "disco"; -- Service discovery

        -- Not essential, but recommended
                "carbons"; -- Keep multiple clients in sync
                "pep"; -- Enables users to publish their avatar, mood, activity, playing music and more
                "private"; -- Private XML storage (for room bookmarks, etc.)
                "blocklist"; -- Allow users to block communications with other users
                "vcard4"; -- User profiles (stored in PEP)
                "vcard_legacy"; -- Conversion between legacy vCard and PEP Avatar, vcard
                "limits"; -- Enable bandwidth limiting for XMPP connections

        -- Nice to have
                "version"; -- Replies to server version requests
                "uptime"; -- Report how long server has been running
                "time"; -- Let others know the time here on this server
                "ping"; -- Replies to XMPP pings with pongs
                "register"; -- Allow users to register on this server using a client and change passwords
                --"mam"; -- Store messages in an archive and allow users to access it
                --"csi_simple"; -- Simple Mobile optimizations

        -- Admin interfaces
                "admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
                --"admin_telnet"; -- Opens telnet console interface on localhost port 5582

        -- HTTP modules
                "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
                --"websocket"; -- XMPP over WebSockets
                --"http_files"; -- Serve static files from a directory over HTTP

        -- Other specific functionality
                --"groups"; -- Shared roster support
                --"server_contact_info"; -- Publish contact information for this service
                --"announce"; -- Send announcement to all online users
                --"welcome"; -- Welcome users who register accounts
                --"watchregistrations"; -- Alert admins of registrations
                --"motd"; -- Send a message to users when they log in
                --"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
                --"proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use
}

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
        -- "offline"; -- Store offline messages
        -- "c2s"; -- Handle client connections
        -- "s2s"; -- Handle server-to-server connections
        -- "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
}

-- Disable account creation by default, for security
-- For more information see https://prosody.im/doc/creating_accounts
allow_registration = true

-- Force clients to use encrypted connections? This option will
-- prevent clients from authenticating unless they are using encryption.

c2s_require_encryption = true

-- Force servers to use encrypted connections? This option will
-- prevent servers from authenticating unless they are using encryption.

s2s_require_encryption = true

-- Force certificate authentication for server-to-server connections?

s2s_secure_auth = false

-- Some servers have invalid or self-signed certificates. You can list
-- remote domains here that will not be required to authenticate using
-- certificates. They will be authenticated using DNS instead, even
-- when s2s_secure_auth is enabled.

--s2s_insecure_domains = { "insecure.example" }

-- Even if you disable s2s_secure_auth, you can still require valid
-- certificates for some domains by specifying a list here.

--s2s_secure_domains = { "jabber.org" }

-- Enable rate limits for incoming client and server connections

limits = {
  c2s = {
    rate = "10kb/s";
  };
  s2sin = {
    rate = "30kb/s";
  };
}

-- Select the authentication backend to use. The 'internal' providers
-- use Prosody's configured data storage to store the authentication data.

authentication = "internal_hashed"

-- Select the storage backend to use. By default Prosody uses flat files
-- in its configured data directory, but it also supports more backends
-- through modules. An "sql" backend is included by default, but requires
-- additional dependencies. See https://prosody.im/doc/storage for more info.

--storage = "sql" -- Default is "internal" (Note: "sql" requires installed
-- lua-dbi RPM package)

-- For the "sql" backend, you can uncomment *one* of the below to configure:
--sql = { driver = "SQLite3", database = "prosody.sqlite" } -- Default. 'database' is the filename.
--sql = { driver = "MySQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }
--sql = { driver = "PostgreSQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }


-- Archiving configuration
-- If mod_mam is enabled, Prosody will store a copy of every message. This
-- is used to synchronize conversations between multiple clients, even if
-- they are offline. This setting controls how long Prosody will keep
-- messages in the archive before removing them.

archive_expires_after = "1w" -- Remove archived messages after 1 week

-- You can also configure messages to be stored in-memory only. For more
-- archiving options, see https://prosody.im/doc/modules/mod_mam

-- Logging configuration
-- For advanced logging see https://prosody.im/doc/logging
log = {
        -- Log everything of level "info" and higher (that is, all except "debug" messages)
        -- to /var/log/prosody/prosody.log and errors also to /var/log/prosody/prosody.err
        info = "/var/log/prosody/prosody.log"; -- Change 'info' to 'debug' for verbose logging
        error = "/var/log/prosody/prosody.err"; -- Log errors also to file
        -- error = "*syslog"; -- Log errors also to syslog
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
}

-- Uncomment to enable statistics
-- For more info see https://prosody.im/doc/statistics
-- statistics = "internal"

-- Certificates
-- Every virtual host and component needs a certificate so that clients and
-- servers can securely verify its identity. Prosody will automatically load
-- certificates/keys from the directory specified here.
-- For more information, including how to use 'prosodyctl' to auto-import certificates
-- (from e.g. Let's Encrypt) see https://prosody.im/doc/certificates

-- Location of directory to find certificates in (relative to main config file):
certificates = "/etc/pki/prosody/"

-- HTTPS currently only supports a single certificate, specify it here:
--https_certificate = "/etc/prosody/certs/localhost.crt"

-- POSIX configuration
-- For more info see https://prosody.im/doc/modules/mod_posix
pidfile = "/run/prosody/prosody.pid";
--daemonize = false -- Default is "true"

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost "${1}"
        enabled = true


                key = "/var/lib/prosody/${1}.key"
                certificate = "/var/lib/prosody/${1}.crt"


--VirtualHost "example.com"
--      certificate = "/path/to/example.crt"

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see https://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.example.com:
Component "conference.${3}.com" "muc"
        restrict_room_creation = "admin"

--- Store MUC messages in an archive and allow users to access it
--modules_enabled = { "muc_mam" }

---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: https://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.example.com"
--      component_secret = "password"

------ Additional config files ------
-- For organizational purposes you may prefer to add VirtualHost and
-- Component definitions in their own config files. This line includes
-- all config files in /etc/prosody/conf.d/

Include "conf.d/*.cfg.lua" " > /etc/prosody/prosody.cfg.lua
sudo prosodyctl register ${2} ${1} ${4}
sudo systemctl start prosody
sudo systemctl status prosody
sudo firewall-cmd --add-port=5222/tcp --permanent
sudo firewall-cmd --add-port=5269/tcp --permanent
sudo firewall-cmd --add-port=5280/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
echo "sucesful configuration"
```
- le script est honteux mais il marche

## III Monitoring 

- J'ai utiliser netdata pour moniturer le serveur on peut voir que le service est demmaré et qu'il est configuré pour ce lancer au demmarage 
```
[kalop@prosody ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-17 00:56:53 CET; 59s ago
  Process: 5870 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/run/netdata (code=exited, status=0/SUCCESS)
  Process: 5868 ExecStartPre=/bin/mkdir -p /opt/netdata/var/run/netdata (code=exited, status=0/SUCCESS)
  Process: 5866 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/cache/netdata (code=exited, status=0/SUCCES>
  Process: 5865 ExecStartPre=/bin/mkdir -p /opt/netdata/var/cache/netdata (code=exited, status=0/SUCCESS)
 Main PID: 5872 (netdata)
 ```
 
 ```
 [kalop@prosody ~]$ sudo systemctl is-enabled netdata
enabled
 ```
 